﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Models
{
    public class Scrap
    {
        public string UrlId { get; set; }
        public IEnumerable<string> ResultadoCompleto { get; set; }
    }
}
