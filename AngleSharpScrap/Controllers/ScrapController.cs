﻿using AngleSharpScrap.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScrapController : ControllerBase
    {
        private readonly ILogger<ScrapController> _logger;
        private readonly ScrapService scrapService;

        public ScrapController(ILogger<ScrapController> logger)
        {
            _logger = logger;
            scrapService = new ScrapService();
        }

        [HttpGet("steam/{urlId}")]
        public async Task<IActionResult> GetAll(string urlId)
        {
            try
            {
                var response = await scrapService.Get(urlId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("ml/{pesquisa}")]
        public async Task<IActionResult> GetAllItems(string pesquisa)
        {
            try
            {
                var response = await scrapService.GetAllItems(pesquisa);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpGet("g1")]
        public async Task<IActionResult> GetAllItems()
        {
            try
            {
                var response = await scrapService.GetAllItemsG1();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
