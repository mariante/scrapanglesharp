﻿using AngleSharp;
using AngleSharp.Html.Dom;
using AngleSharpScrap.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Repositories
{
    public class ScrapRepository
    {
        public async Task<Scrap> Get(string urlId)
        {
            //Cria a config com cookies;
            var config = Configuration.Default.WithDefaultLoader().WithDefaultCookies();
            
            //Invoca a config para iniciar;
            var context = BrowsingContext.New(config);
            
            //Inicia operação assincrona para entrar no site;
            var queryDocument = await context.OpenAsync("https://www.steamidfinder.com/");
            
            //Faz a busca por um FORM no site;
            IHtmlFormElement form = (IHtmlFormElement)queryDocument.QuerySelector("form");
            
            //Preenche o FORM com o INPUT único de id steamid com o parametro urlId;
            var resultDocument = await form.SubmitAsync(new { steamid = urlId });
            
            //Recebe os resultados, apresentados dentro da tag <code> no html;
            var resultadoCompleto = resultDocument.QuerySelectorAll("code").Select(m => m.TextContent);
            
            //Cria o model para retornar ao body;
            return new Scrap
            {
                UrlId = urlId,
                ResultadoCompleto = resultadoCompleto
            };
        }
    }
}
