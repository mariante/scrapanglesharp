﻿using AngleSharp;
using AngleSharp.Html.Dom;
using AngleSharpScrap.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Repositories
{
    public class MlRepository
    {
        public async Task<List<Ml>> GetAllItems(string pesquisa)
        {
            List<Ml> ml = new List<Ml>();
            //Cria a config com cookies;
            var config = Configuration.Default.WithDefaultLoader().WithDefaultCookies();
            
            //Invoca a config para iniciar;
            var context = BrowsingContext.New(config);
            
            //Inicia operação assincrona para entrar no site;
            var queryDocument = await context.OpenAsync("https://www.mercadolivre.com.br/");

            //var form = queryDocument.All.Where(m => m.LocalName == "form" && m.ClassList.Contains("nav-search"));
            IHtmlFormElement form = (IHtmlFormElement)queryDocument.QuerySelector("form");
            var submit = await form.SubmitAsync(
                new {
                    as_word = pesquisa
                });
            var textContent = submit.QuerySelectorAll("li").Where(c => c.ClassList.Contains("ui-search-layout__item")).ToList();
            foreach (var tc in textContent)
            {
                var newML = new Ml
                {
                    TextContent = tc.TextContent
                };
                ml.Add(newML);
            }
            return ml;
        }
    }
}
