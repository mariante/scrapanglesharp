﻿using AngleSharp;
using AngleSharp.Html.Dom;
using AngleSharpScrap.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Repositories
{
    public class Ge1Repository
    {
        public async Task<List<G1>> GetAllItemsG1()
        {
            List<G1> g1 = new List<G1>();
            //Cria a config com cookies;
            var config = Configuration.Default.WithDefaultLoader().WithDefaultCookies();
            
            //Invoca a config para iniciar;
            var context = BrowsingContext.New(config);
            
            //Inicia operação assincrona para entrar no site;
            var queryDocument = await context.OpenAsync("https://g1.globo.com/");
            var getInfo = queryDocument.All.Where(m => m.LocalName == "div" && m.ClassList.Contains("feed-post-body")).ToList();

            foreach (var tc in getInfo)
            {
                var newG1 = new G1
                {
                    TextContent = tc.TextContent
                };
                g1.Add(newG1);
            }
            return g1;
        }
    }
}
