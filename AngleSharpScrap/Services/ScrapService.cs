﻿using AngleSharpScrap.Models;
using AngleSharpScrap.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngleSharpScrap.Services
{
    public class ScrapService
    {
        private readonly ScrapRepository _scrapRepository = new ScrapRepository();
        private readonly MlRepository _mlRepository = new MlRepository();
        private readonly Ge1Repository _g1Repository = new Ge1Repository();
        public Task<Scrap> Get(string urlId)
        {
            return _scrapRepository.Get(urlId);
        }
        public Task<List<Ml>> GetAllItems(string pesquisa)
        {
            return _mlRepository.GetAllItems(pesquisa);
        }
        public Task<List<G1>> GetAllItemsG1()
        {
            return _g1Repository.GetAllItemsG1();
        }
    }
}
